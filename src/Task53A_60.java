import models.Date;

public class Task53A_60 {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date();
        date1.setDate(8, 6, 1995);

        Date date2 = new Date(31, 12, 2022);

        System.out.println(date1.toString());
        System.out.println(date2.toString());
    }
}
