import models.InvoiceItem;

public class Task53A_40 {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem("001", "Sách", 2, 150000);
        InvoiceItem invoiceItem2 = new InvoiceItem("002", "Bút", 5, 20000);

        // In thông tin đối tượng invoiceItem1 và invoiceItem2 ra console
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());

        // Tính và in tổng giá của 2 đối tượng invoiceItem1 và invoiceItem2 ra console
        double total1 = invoiceItem1.getTotal();
        double total2 = invoiceItem2.getTotal();
        System.out.println("\nTổng giá hóa đơn 1: " + total1);
        System.out.println("Tổng giá hóa đơn 2: " + total2);
    }
}
