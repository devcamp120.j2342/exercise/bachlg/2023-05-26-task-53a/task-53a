import models.Rectangle;

public class Task53A_20 {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        System.out.println("Rectangle 1:");
        System.out.println(rectangle1);
        System.out.println("Area: " + rectangle1.getArea());
        System.out.println("Perimeter: " + rectangle1.getPerimeter());

        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println("\nRectangle 2:");
        System.out.println(rectangle2);
        System.out.println("Area: " + rectangle2.getArea());
        System.out.println("Perimeter: " + rectangle2.getPerimeter());
    }
}


