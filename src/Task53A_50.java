import models.Account;

public class Task53A_50 {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("1", "Account 1");
        Account account2 = new Account("2", "Account 2", 1000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // Tăng số dư cho tài khoản 1 và tài khoản 2
        account1.credit(2000);
        account2.credit(3000);

        System.out.println("\nThông tin tài khoản sau khi tăng số dư:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // Giảm số dư cho tài khoản 1 và tài khoản 2
        account1.debit(1000);
        account2.debit(5000);

        System.out.println("\nThông tin tài khoản sau khi giảm số dư:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // Chuyển khoản từ tài khoản 1 sang tài khoản 2
        account1.transferTo(account2, 2000);

        System.out.println("\nThông tin tài khoản sau khi chuyển khoản từ tài khoản 1 sang tài khoản 2:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // Chuyển khoản từ tài khoản 2 sang tài khoản 1
        account2.transferTo(account1, 2000);

        System.out.println("\nThông tin tài khoản sau khi chuyển khoản từ tài khoản 2 sang tài khoản 1:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}
