import models.Employee;

public class Task53A_30 {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "John", "Doe", 5000);
        Employee employee2 = new Employee(2, "Jane", "Doe", 6000);

        // In thông tin của 2 đối tượng nhân viên
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        // In tên đầy đủ và lương 1 năm của 2 nhân viên
        System.out.println("\nTên đầy đủ của nhân viên 1: " + employee1.getName());
        System.out.println("Lương 1 năm của nhân viên 1: " + employee1.getAnnualSalary());
        System.out.println("\nTên đầy đủ của nhân viên 2: " + employee2.getName());
        System.out.println("Lương 1 năm của nhân viên 2: " + employee2.getAnnualSalary());

        // In lại lương sau khi tăng của 2 nhân viên
        System.out.println("\nLương của nhân viên 1 sau khi tăng: " + employee1.raiseSalary(10));
        System.out.println("Lương của nhân viên 2 sau khi tăng: " + employee2.raiseSalary(15));
    }
}
